#------------------------------------------------------------------------------+
#
#   Nathan A. Rooy
#   Simple Particle Swarm Optimization (PSO) with Python
#   July, 2016
#
#------------------------------------------------------------------------------+

#--- IMPORT DEPENDENCIES ------------------------------------------------------+

from __future__ import division
import random
import math
import numpy as np
import operator

#--- COST FUNCTION ------------------------------------------------------------+

# function we are attempting to optimize (minimize)
def sphere(x):
    total=0
    for i in range(len(x)):
        total+=x[i]**2
    return total

def rastring(x):
    y = 10 * len(x) + sum(map(lambda i: i**2 - 10 * np.cos(2*np.pi*i), x))
    return y

def ackley(x):
    dim = len(x)
    term1 = -1. * 20 * np.exp(-1. * 0.2 * np.sqrt((1./dim) * sum(map(lambda i: i**2, x))))
    term2 = -1. * np.exp((1./dim) * (sum(map(lambda j: np.cos(2*np.pi * j), x))))        
    return term1 + term2 + 20 + np.exp(1)

#--- MAIN ---------------------------------------------------------------------+

class Particle:
    def __init__(self,x0):
        self.position_i=[]          # particle position
        self.velocity_i=[]          # particle velocity
        self.pos_best_i=[]          # best position individual
        self.fitness_best_i=-1      # best fitness individual
        self.fitness_i=-1           # fitness individual
        self.neighborhood = []      # neighborhood (2 neighbors)

        for i in range(0,num_dimensions):
            self.velocity_i.append(random.uniform(-1,1))
            self.position_i.append(x0[i])

    # evaluate current fitness
    def evaluate(self,costFunc):
        self.fitness_i=costFunc(self.position_i)

        # check to see if the current position is an individual best
        if self.fitness_i<self.fitness_best_i or self.fitness_best_i==-1:
            self.pos_best_i=self.position_i
            self.fitness_best_i=self.fitness_i
                    
    # update new particle velocity
    def update_velocity(self,pos_best_g):
        #w=0.5       # constant inertia weight (how much to weigh the previous velocity)
        c1=2        # cognitive constant
        c2=2        # social constant
        phi = c1+c2
        k = 2/abs(2-phi-math.sqrt(phi**2 - 4*phi))
        
        for i in range(0,num_dimensions):
            r1=random.random()
            r2=random.random()
            
            vel_cognitive=c1*r1*(self.pos_best_i[i]-self.position_i[i])
            vel_social=c2*r2*(pos_best_g[i]-self.position_i[i])
            self.velocity_i[i]=k*(self.velocity_i[i]+vel_cognitive+vel_social)

    # update the particle position based off new velocity updates
    def update_position(self,bounds):
        for i in range(0,num_dimensions):
            self.position_i[i]=self.position_i[i]+self.velocity_i[i]
            
            # adjust maximum position if necessary
            if self.position_i[i]>bounds[i][1]:
                self.position_i[i]=bounds[i][1]

            # adjust minimum position if neseccary
            if self.position_i[i]<bounds[i][0]:
                self.position_i[i]=bounds[i][0]

    def neighborhood_find(self, swarm, num_neighbors):
        list_aux = []
        self.neighborhood = []
        for j in range(len(swarm)):
            if(self != swarm[j]):
                total = 0
                for k in range(len(bounds)):
                    total += abs(self.position_i[k] - swarm[j].position_i[k])
                list_aux.append([total,swarm[j]])
        list_aux.sort(key = operator.itemgetter(0))
        for i in range(num_neighbors):
            self.neighborhood.append(list_aux[i][1])
        
class PSO():

    def __init__(self,costFunc,x0,bounds,num_particles,maxiter,mode):
        global num_dimensions
        num_dimensions=len(x0)
        fitness_best_g=-1               # best fitness for group
        pos_best_g=[]                   # best position for group
        # establish the swarm
        swarm=[]   
        for i in range(0,num_particles):
            swarm.append(Particle(x0))

        # begin optimization loop
        i=0

        if mode == 'local':
            while i<maxiter:
                for particle in swarm:
                    particle.evaluate(costFunc)
                    fitness_best_l = particle.fitness_best_i
                    pos_best_l = particle.pos_best_i
                    #print (i,fitness_best_g, pos_best_g)
                    particle.neighborhood_find(swarm,num_neighbors=2)

                    # cycle through particles in swarm and evaluate fitness
                    for neighborhood in particle.neighborhood:
                        neighborhood.evaluate(costFunc)

                    # determine if current particle is the best (globally)
                    if neighborhood.fitness_i<fitness_best_l:
                        pos_best_l=list(neighborhood.position_i)
                        fitness_best_l=float(neighborhood.fitness_i)
                    particle.update_velocity(pos_best_l)
                    particle.update_position(bounds)

                    if particle.fitness_i<fitness_best_g or fitness_best_g==-1:
                        pos_best_g=list(particle.position_i)
                        fitness_best_g=float(particle.fitness_i)
                i+=1
        
        elif mode == 'global':
            while i<maxiter:
                print (i,fitness_best_g)
                # cycle through particles in swarm and evaluate fitness
                for j in range(0,num_particles):
                    swarm[j].evaluate(costFunc)

                    # determine if current particle is the best (globally)
                    if swarm[j].fitness_i<fitness_best_g or fitness_best_g==-1:
                        pos_best_g=list(swarm[j].position_i)
                        fitness_best_g=float(swarm[j].fitness_i)
                
                # cycle through swarm and update velocities and position
                for j in range(0,num_particles):
                    swarm[j].update_velocity(pos_best_g)
                    swarm[j].update_position(bounds)
                i+=1

        # print final results
        print ('FINAL:')
        print (pos_best_g)
        print (fitness_best_g)


if __name__ == "__PSO__":
    main()

#--- RUN ----------------------------------------------------------------------+

initial=[2.5,1.5]               # initial starting location [x1,x2...]
bounds=[(-4,4),(-4,4)]  # input bounds [(x1_min,x1_max),(x2_min,x2_max)...]
PSO(ackley,initial,bounds,num_particles=15,maxiter=50,mode='local')

#--- END ----------------------------------------------------------------------+
